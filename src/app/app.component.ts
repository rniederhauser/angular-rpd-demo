import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import Rpd from 'rpd';
const shared = require('rpd/src/render/shared');
const quartz = require('rpd/src/style/quartz/html');
const test = require('rpd/src/render/html');
import Kefir from 'kefir';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'rpd-demo';

  @ViewChild('target1', { static: false })
  target1: ElementRef;

  @ViewChild('target2', { static: false })
  target2: ElementRef;


  ngAfterViewInit(): void {
    Rpd.renderNext('html', this.target1.nativeElement, { style: 'quartz' });
    const firstTargetOne = Rpd.addPatch('target-1-1');
    firstTargetOne.addNode('util/random');
    firstTargetOne.addNode('util/random');
    const firstTargetTwo = Rpd.addPatch('target-1-2');
    firstTargetTwo.addNode('util/random');
    firstTargetTwo.addNode('util/random');
    Rpd.renderNext('html', this.target2.nativeElement, { style: 'quartz' });
    const secondTargetOne = Rpd.addPatch('target-2-1');
    secondTargetOne.addNode('util/random');
    const secondTargetTwo = Rpd.addClosedPatch('target-2-2');
    secondTargetTwo.addNode('util/random');
  }
}
